<?php

namespace BackendBundle\Entity;

/**
 * SportStat
 */
class SportStat
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $chort;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \BackendBundle\Entity\Sport
     */
    private $sport;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SportStat
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set chort
     *
     * @param string $chort
     *
     * @return SportStat
     */
    public function setChort($chort)
    {
        $this->chort = $chort;

        return $this;
    }

    /**
     * Get chort
     *
     * @return string
     */
    public function getChort()
    {
        return $this->chort;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return SportStat
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set sport
     *
     * @param \BackendBundle\Entity\Sport $sport
     *
     * @return SportStat
     */
    public function setSport(\BackendBundle\Entity\Sport $sport = null)
    {
        $this->sport = $sport;

        return $this;
    }

    /**
     * Get sport
     *
     * @return \BackendBundle\Entity\Sport
     */
    public function getSport()
    {
        return $this->sport;
    }
}
