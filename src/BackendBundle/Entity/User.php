<?php

use Symfony\Component\Serializer\Annotation\MaxDepth;
namespace BackendBundle\Entity;

/**
 * User
 */
class User
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $role;

    /**
     * @var string
     */
    private $run;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $lastname;

    /**
     * @var string
     */
    private $email;

    /**
     * @var integer
     */
    private $cellnumber;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $image;

    /**
     * @var \DateTime
     */
    private $birthDate;

    /**
     * @var string
     */
    private $origin;

    /**
     * @var string
     */
    private $nacionality;

    /**
     * @var string
     */
    private $originClub;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var string
     */
    private $status;

    /**
     * @var \BackendBundle\Entity\Club
     */
    private $club;

    /**
     * @var \Doctrine\Common\Collections\Collection
	 * @MaxDepth(1)
     */
    private $sports;
	
	/**
     * @var \Doctrine\Common\Collections\Collection
	 * @MaxDepth(1)
     */
    private $teams;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sports = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set run
     *
     * @param string $run
     *
     * @return User
     */
    public function setRun($run)
    {
        $this->run = $run;

        return $this;
    }

    /**
     * Get run
     *
     * @return string
     */
    public function getRun()
    {
        return $this->run;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set cellnumber
     *
     * @param integer $cellnumber
     *
     * @return User
     */
    public function setCellnumber($cellnumber)
    {
        $this->cellnumber = $cellnumber;

        return $this;
    }

    /**
     * Get cellnumber
     *
     * @return integer
     */
    public function getCellnumber()
    {
        return $this->cellnumber;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return User
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     *
     * @return User
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set origin
     *
     * @param string $origin
     *
     * @return User
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;

        return $this;
    }

    /**
     * Get origin
     *
     * @return string
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * Set nacionality
     *
     * @param string $nacionality
     *
     * @return User
     */
    public function setNacionality($nacionality)
    {
        $this->nacionality = $nacionality;

        return $this;
    }

    /**
     * Get nacionality
     *
     * @return string
     */
    public function getNacionality()
    {
        return $this->nacionality;
    }

    /**
     * Set originClub
     *
     * @param string $originClub
     *
     * @return User
     */
    public function setOriginClub($originClub)
    {
        $this->originClub = $originClub;

        return $this;
    }

    /**
     * Get originClub
     *
     * @return string
     */
    public function getOriginClub()
    {
        return $this->originClub;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set club
     *
     * @param \BackendBundle\Entity\Club $club
     *
     * @return User
     */
    public function setClub(\BackendBundle\Entity\Club $club = null)
    {
        $this->club = $club;

        return $this;
    }

    /**
     * Get club
     *
     * @return \BackendBundle\Entity\Club
     */
    public function getClub()
    {
        return $this->club;	
    }	

    /**
     * Add sport
     *
     * @param \BackendBundle\Entity\Sport $sport
     *
     * @return User
     */
    public function addSport(\BackendBundle\Entity\Sport $sport)
    {
		$this->sports[] = $sport;

		return $this;
    }

    /**
     * Remove sport
     *
     * @param \BackendBundle\Entity\Sport $sport
     */
    public function removeSport(\BackendBundle\Entity\Sport $sport)
    {
        $this->sports->removeElement($sport);
    }

    /**
     * Get sports
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSports()
    {	
        return $this->sports;
    }
	
	/**
     * Add team
     *
     * @param \BackendBundle\Entity\Sport $sport
     *
     * @return User
     */
    public function addTeam(\BackendBundle\Entity\Team $team)
    {
		$this->teams[] = $team;

		return $this;
    }

    /**
     * Remove sport
     *
     * @param \BackendBundle\Entity\Team $team
     */
    public function removeTeam(\BackendBundle\Entity\Team $team)
    {
        $this->teams->removeElement($team);
    }

    /**
     * Get sports
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTeams()
    {	
//		$teams[];
//		foreach($this->teams as $t) {
//			$teams[] = 1;
//		}
//        return $teams;
		return $this->teams;
    }
		
}
