<?php

namespace BackendBundle\Entity;

/**
 * FotballPlStats
 */
class FotballPlStats
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $position;

    /**
     * @var integer
     */
    private $games = '0';

    /**
     * @var integer
     */
    private $titular = '0';

    /**
     * @var integer
     */
    private $alternate = '0';

    /**
     * @var integer
     */
    private $notPlay = '0';

    /**
     * @var integer
     */
    private $notGame = '0';

    /**
     * @var integer
     */
    private $minutes = '0';

    /**
     * @var integer
     */
    private $goals = '0';

    /**
     * @var integer
     */
    private $assists = '0';

    /**
     * @var integer
     */
    private $passes = '0';

    /**
     * @var integer
     */
    private $yellowT = '0';

    /**
     * @var integer
     */
    private $redT = '0';

    /**
     * @var float
     */
    private $assessment = '0';

    /**
     * @var \BackendBundle\Entity\Sport
     */
    private $sport;

    /**
     * @var \BackendBundle\Entity\Team
     */
    private $team;

    /**
     * @var \BackendBundle\Entity\User
     */
    private $user;
	

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return FotballPlStats
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set games
     *
     * @param integer $games
     *
     * @return FotballPlStats
     */
    public function setGames($games)
    {
        $this->games = $games;

        return $this;
    }

    /**
     * Get games
     *
     * @return integer
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * Set titular
     *
     * @param integer $titular
     *
     * @return FotballPlStats
     */
    public function setTitular($titular)
    {
        $this->titular = $titular;

        return $this;
    }

    /**
     * Get titular
     *
     * @return integer
     */
    public function getTitular()
    {
        return $this->titular;
    }

    /**
     * Set alternate
     *
     * @param integer $alternate
     *
     * @return FotballPlStats
     */
    public function setAlternate($alternate)
    {
        $this->alternate = $alternate;

        return $this;
    }

    /**
     * Get alternate
     *
     * @return integer
     */
    public function getAlternate()
    {
        return $this->alternate;
    }

    /**
     * Set notPlay
     *
     * @param integer $notPlay
     *
     * @return FotballPlStats
     */
    public function setNotPlay($notPlay)
    {
        $this->notPlay = $notPlay;

        return $this;
    }

    /**
     * Get notPlay
     *
     * @return integer
     */
    public function getNotPlay()
    {
        return $this->notPlay;
    }

    /**
     * Set notGame
     *
     * @param integer $notGame
     *
     * @return FotballPlStats
     */
    public function setNotGame($notGame)
    {
        $this->notGame = $notGame;

        return $this;
    }

    /**
     * Get notGame
     *
     * @return integer
     */
    public function getNotGame()
    {
        return $this->notGame;
    }

    /**
     * Set minutes
     *
     * @param integer $minutes
     *
     * @return FotballPlStats
     */
    public function setMinutes($minutes)
    {
        $this->minutes = $minutes;

        return $this;
    }

    /**
     * Get minutes
     *
     * @return integer
     */
    public function getMinutes()
    {
        return $this->minutes;
    }

    /**
     * Set goals
     *
     * @param integer $goals
     *
     * @return FotballPlStats
     */
    public function setGoals($goals)
    {
        $this->goals = $goals;

        return $this;
    }

    /**
     * Get goals
     *
     * @return integer
     */
    public function getGoals()
    {
        return $this->goals;
    }

    /**
     * Set assists
     *
     * @param integer $assists
     *
     * @return FotballPlStats
     */
    public function setAssists($assists)
    {
        $this->assists = $assists;

        return $this;
    }

    /**
     * Get assists
     *
     * @return integer
     */
    public function getAssists()
    {
        return $this->assists;
    }

    /**
     * Set passes
     *
     * @param integer $passes
     *
     * @return FotballPlStats
     */
    public function setPasses($passes)
    {
        $this->passes = $passes;

        return $this;
    }

    /**
     * Get passes
     *
     * @return integer
     */
    public function getPasses()
    {
        return $this->passes;
    }

    /**
     * Set yellowT
     *
     * @param integer $yellowT
     *
     * @return FotballPlStats
     */
    public function setYellowT($yellowT)
    {
        $this->yellowT = $yellowT;

        return $this;
    }

    /**
     * Get yellowT
     *
     * @return integer
     */
    public function getYellowT()
    {
        return $this->yellowT;
    }

    /**
     * Set redT
     *
     * @param integer $redT
     *
     * @return FotballPlStats
     */
    public function setRedT($redT)
    {
        $this->redT = $redT;

        return $this;
    }

    /**
     * Get redT
     *
     * @return integer
     */
    public function getRedT()
    {
        return $this->redT;
    }

    /**
     * Set assessment
     *
     * @param float $assessment
     *
     * @return FotballPlStats
     */
    public function setAssessment($assessment)
    {
        $this->assessment = $assessment;

        return $this;
    }

    /**
     * Get assessment
     *
     * @return float
     */
    public function getAssessment()
    {
        return $this->assessment;
    }

    /**
     * Set sport
     *
     * @param \BackendBundle\Entity\Sport $sport
     *
     * @return FotballPlStats
     */
    public function setSport(\BackendBundle\Entity\Sport $sport = null)
    {
        $this->sport = $sport;

        return $this;
    }

    /**
     * Get sport
     *
     * @return \BackendBundle\Entity\Sport
     */
    public function getSport()
    {
        return $this->sport;
    }

    /**
     * Set team
     *
     * @param \BackendBundle\Entity\Team $team
     *
     * @return FotballPlStats
     */
    public function setTeam(\BackendBundle\Entity\Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \BackendBundle\Entity\Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set user
     *
     * @param \BackendBundle\Entity\User $user
     *
     * @return FotballPlStats
     */
    public function setUser(\BackendBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BackendBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
