<?php

namespace BackendBundle\Entity;

/**
 * Team
 */
class Team
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $image;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var string
     */
    private $status;

    /**
     * @var \BackendBundle\Entity\Sport
     */
    private $sport;
	
    /**
     * @var \BackendBundle\Entity\User
     */
    private $creator;
	
	/**
     * @var \BackendBundle\Entity\Club
     */
    private $club;
	
	/**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $users;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Team
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Team
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Team
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Team
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Team
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set sport
     *
     * @param \BackendBundle\Entity\Sport $sport
     *
     * @return Team
     */
    public function setSport(\BackendBundle\Entity\Sport $sport = null)
    {
        $this->sport = $sport;

        return $this;
    }

    /**
     * Get sport
     *
     * @return \BackendBundle\Entity\Sport
     */
    public function getSport()
    {
        return $this->sport;
    }


    /**
     * Set creator
     *
     * @param integer $creator
     *
     * @return Team
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return integer
     */
    public function getCreator()
    {
        return $this->creator;
    }
	
	/**
     * Set Club
     *
     * @param integer $club
     *
     * @return Team
     */
    public function setClub($club)
    {
        $this->club = $club;

        return $this;
    }

    /**
     * Get Club
     *
     * @return \BackendBundle\Entity\Club
     */
    public function getClub()
    {
        return $this->club;
    }
	
	/**
     * Add user
     *
     * @param \BackendBundle\Entity\Sport $user
     *
     * @return Team
     */
	public function addUser(\BackendBundle\Entity\User $user)
    {
		$this->users[] = $user;

		return $this;
    }

    /**
     * Remove user
     *
     * @param \BackendBundle\Entity\User $user
     */
    public function removeUser(\BackendBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {	
        return $this->users;
    }

}
