<?php

namespace BackendBundle\Entity;

/**
 * FotballPlGameStats
 */
class FotballPlGameStats
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $position;

    /**
     * @var integer
     */
    private $minutes;

    /**
     * @var integer
     */
    private $goals;

    /**
     * @var integer
     */
    private $assists;

    /**
     * @var integer
     */
    private $passes;

    /**
     * @var integer
     */
    private $yellowT;

    /**
     * @var integer
     */
    private $redT;

    /**
     * @var float
     */
    private $assessment;

    /**
     * @var integer
     */
    private $gameId;

    /**
     * @var \BackendBundle\Entity\FotballPlStats
     */
    private $player;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return FotballPlGameStats
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set minutes
     *
     * @param integer $minutes
     *
     * @return FotballPlGameStats
     */
    public function setMinutes($minutes)
    {
        $this->minutes = $minutes;

        return $this;
    }

    /**
     * Get minutes
     *
     * @return integer
     */
    public function getMinutes()
    {
        return $this->minutes;
    }

    /**
     * Set goals
     *
     * @param integer $goals
     *
     * @return FotballPlGameStats
     */
    public function setGoals($goals)
    {
        $this->goals = $goals;

        return $this;
    }

    /**
     * Get goals
     *
     * @return integer
     */
    public function getGoals()
    {
        return $this->goals;
    }

    /**
     * Set assists
     *
     * @param integer $assists
     *
     * @return FotballPlGameStats
     */
    public function setAssists($assists)
    {
        $this->assists = $assists;

        return $this;
    }

    /**
     * Get assists
     *
     * @return integer
     */
    public function getAssists()
    {
        return $this->assists;
    }

    /**
     * Set passes
     *
     * @param integer $passes
     *
     * @return FotballPlGameStats
     */
    public function setPasses($passes)
    {
        $this->passes = $passes;

        return $this;
    }

    /**
     * Get passes
     *
     * @return integer
     */
    public function getPasses()
    {
        return $this->passes;
    }

    /**
     * Set yellowT
     *
     * @param integer $yellowT
     *
     * @return FotballPlGameStats
     */
    public function setYellowT($yellowT)
    {
        $this->yellowT = $yellowT;

        return $this;
    }

    /**
     * Get yellowT
     *
     * @return integer
     */
    public function getYellowT()
    {
        return $this->yellowT;
    }

    /**
     * Set redT
     *
     * @param integer $redT
     *
     * @return FotballPlGameStats
     */
    public function setRedT($redT)
    {
        $this->redT = $redT;

        return $this;
    }

    /**
     * Get redT
     *
     * @return integer
     */
    public function getRedT()
    {
        return $this->redT;
    }

    /**
     * Set assessment
     *
     * @param float $assessment
     *
     * @return FotballPlGameStats
     */
    public function setAssessment($assessment)
    {
        $this->assessment = $assessment;

        return $this;
    }

    /**
     * Get assessment
     *
     * @return float
     */
    public function getAssessment()
    {
        return $this->assessment;
    }

    /**
     * Set gameId
     *
     * @param integer $gameId
     *
     * @return FotballPlGameStats
     */
    public function setGameId($gameId)
    {
        $this->gameId = $gameId;

        return $this;
    }

    /**
     * Get gameId
     *
     * @return integer
     */
    public function getGameId()
    {
        return $this->gameId;
    }

    /**
     * Set player
     *
     * @param \BackendBundle\Entity\FotballPlStats $player
     *
     * @return FotballPlGameStats
     */
    public function setPlayer(\BackendBundle\Entity\FotballPlStats $player = null)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return \BackendBundle\Entity\FotballPlStats
     */
    public function getPlayer()
    {
        return $this->player;
    }
}
