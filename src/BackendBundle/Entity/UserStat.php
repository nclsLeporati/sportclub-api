<?php

namespace BackendBundle\Entity;

/**
 * UserStat
 */
class UserStat
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $value;

    /**
     * @var \BackendBundle\Entity\SportStat
     */
    private $sportStat;

    /**
     * @var \BackendBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return UserStat
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set sportStat
     *
     * @param \BackendBundle\Entity\SportStat $sportStat
     *
     * @return UserStat
     */
    public function setSportStat(\BackendBundle\Entity\SportStat $sportStat = null)
    {
        $this->sportStat = $sportStat;

        return $this;
    }

    /**
     * Get sportStat
     *
     * @return \BackendBundle\Entity\SportStat
     */
    public function getSportStat()
    {
        return $this->sportStat;
    }

    /**
     * Set user
     *
     * @param \BackendBundle\Entity\User $user
     *
     * @return UserStat
     */
    public function setUser(\BackendBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BackendBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
