<?php

namespace BackendBundle\Repository;

use BackendBundle\Entity\Traing;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class TraingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Traing::class);
    }
	
	public function findAllActived($sport): array
		{
			// automatically knows to select Products
			// the "p" is an alias you'll use in the rest of the query
			$qb = $this->createQueryBuilder('t')
				->andWhere('t.status != deleted')
				->setParameter('sport', $sport)
				->orderBy('t.id', 'DESC')
				->getQuery();

			return $qb->execute();

			// to get just one result:
			// $product = $qb->setMaxResults(1)->getOneOrNullResult();
		}
}