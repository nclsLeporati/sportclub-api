<?php

namespace BackendBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class UserRepository extends EntityRepository
{
//    public function __construct(RegistryInterface $registry)
//    {
//        parent::__construct($registry, User::class);
//    }
	

		
	public function findUsersBySportRoleClub($sport, $role, $club)
		{	
			$qb = $this->createQueryBuilder("u")
				->where(':sport MEMBER OF u.sports')
				->andWhere("u.role = :role")
				->andWhere("u.club = :club")
				->setParameters(array(
					'sport' => $sport,
					'role' => $role,
					'club' => $club
					));
			
			return $qb->getQuery()->getResult();
		}
		
	public function findUsersBySport($params)
		{	
			$qb = $this->createQueryBuilder("u")
				->where('1=1');
			
			if (!empty($params['sport'])) {
				$qb->andWhere(':sport MEMBER OF u.sports');
			}
			
			if (!empty($params['team'])) {
				$qb->andWhere(':team MEMBER OF u.teams');
			}
			
			if (!empty($params)) {				
				foreach ($params as $key => $param) {
					if ($key != 'sport' && $key != 'team') {					
						$qb->andWhere("u.".$key." = :".$key);
					}
				}

				$qb->setParameters($params);
			}
//			$qb->setParameters(array('sport' => $params['sport']));
			
			return $qb->getQuery()->getResult();
		}
}