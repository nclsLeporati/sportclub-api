<?php

namespace AppBundle\Services;

use Firebase\JWT\JWT;

class JwtAuth {
	
	public $manager;
	
	public function __construct($manager) {
		$this -> manager = $manager;
	}
	
	public function signup($email, $password, $getHash = null) {
		$key = "secret-pass";
		
		$user = $this -> manager -> getRepository('BackendBundle:User')->findOneBy(
					array(
						"email" => $email,
						"password" => $password
					)
				);
		
		$signup = false;
		if (is_object($user)) {
			$signup = true;
		}
		
		if ($signup == true) {
			
			$token = array(
				"sub" => $user->getId(),
				"run" => $user->getRun(),
				"role" => $user->getRole(),
				"name" => $user->getName(),
				"lastname" => $user->getLastname(),
				"email" => $user->getEmail(),
				"cellnumber" => $user->getCellnumber(),
				"password" => $user->getPassword(),
				"image" => $user->getImage(),
				"birthDate" => $user->getBirthDate(),
				"nacionality" => $user->getNacionality(),
				"origin" => $user->getOrigin(),
				"createdAt" => $user->getCreatedAt(),
				"status" => $user->getStatus(),
				"email" => $user->getEmail(),
				"club" => array("name" => $user->getClub()->getName(), "id" => $user->getClub()->getId()),
				"sports" => $user->getSports(),
				"teams" => $user->getTeams(),
				"iat" => time(),
				"exp" => time() + (7*24*60*60)
			);
			
			$jwt = JWT::encode($token, $key, 'HS256');
			$decoded = JWT::decode($jwt, $key, array('HS256'));
			
			if ($getHash != null) {
				return $jwt;
			} else {
				return $decoded;
			}
			
		} else {
			return array("status" => "error", "data" => "Login failed!!");
		}
	}
	
	public function checkToken($jwt, $getIdentity = false) {
		$key = "secret-pass";
		$auth = false;
		
		try{
			$decoded = JWT::decode($jwt, $key, array('HS256'));
		} catch (\UnexpectedValueException $e) {
			$auth = false;
		} catch (\DomainException $e){
			$auth = false;
		}
		
		if (isset($decoded->sub)) {
			$auth = true;
		} else {
			$auth = false;
		}
		
		if ($getIdentity == true) {
			return $decoded;
		} else {
			return $auth;
		}
	}
}
