<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;
use BackendBundle\Entity\Clndrevent;
use BackendBundle\Entity\User;

class ClndreventController extends Controller {
	
	public function newAction(Request $request) {
		
		$helpers = $this->get("app.helpers");		
		$json = $request->get("json", null);		
		$hash = $request->get("auth");
		$authCheck = $helpers->authcheck($hash);
		
		if ($authCheck) {
			
			$params = json_decode($json);
			
			if ($json != null) {
				
				$em = $this->getDoctrine()->getManager();
				$userId = (isset($params->user_id)) ? $params->user_id : null;
				
				if ($userId != null && $userId > 0) {
					
					$user = $em->getRepository("BackendBundle:User")->findOneBy(array(
						"id" => $userId
					));
					$createdAt = new \Datetime('now');
					$typeId = 0;
					
					//$startDate = (isset($params->start_date)) ? $params->start_date : null;
					$startDate = new \Datetime('now');
					$name = (isset($params->name)) ? $params->name : null;
					$event = (isset($params->event)) ? $params->event : null;
					$status = (isset($params->status)) ? $params->status : "public";
					
					if (
							$name != null && $name != "" 
							&& $event != null) {
						
						$clndrEvent = new Clndrevent();
						$clndrEvent ->setUser($user);
						$clndrEvent ->setStartDate($startDate);
						$clndrEvent ->setCreatedAt($createdAt);
						$clndrEvent ->setName($name);
						$clndrEvent ->setEvent($event);
						$clndrEvent ->setTypeId($typeId);
						$clndrEvent ->setStatus($status);
						
						$em ->persist($clndrEvent);
						$em ->flush();
						$data = array(
							"status" => "success",
							"code" => 200,
							"msg" => "Event created"
						);
						
					} else {
						$data = array(
							"status" => "error",
							"code" => 400,
							"msg" => "params failed"
						);
					}
					
				} else {
					$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "user dont exist"
					);
				}
				
			} else {
				$data = array(
					"status" => "error",
					"code" => 400,
					"msg" => "json failed"
				);
			}
			
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "authorization failed"
			);
		}
		
		return $helpers -> json($data);;
	}
	
	public function detailAction (Request $request, $id=null) {
		
		$helpers = $this->get("app.helpers");			
		$hash = $request->get("auth");
		$authCheck = $helpers->authcheck($hash);
		
		if ($authCheck) {
				
			$em = $this->getDoctrine()->getManager();
			$clndrEvent = $em->getRepository("BackendBundle:Clndrevent")->findOneBy(array(
				"id" => $id
			));

			if ($clndrEvent) {

				$data = array(
					"status" => "success",
					"code" => 200,
					"msg" => "event returned",
					"data" => $clndrEvent
				);

			} else {
				$data = array(
					"status" => "error",
					"code" => 400,
					"msg" => "event dont exist"
				);
			}
			
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "authorization failed"
			);
		}
		
		return $helpers->json($data);
	}
}