<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;
use BackendBundle\Entity\FotballPlStats;

class PlayerStatsController extends Controller {
	
	public function newAction(Request $request) {
		
		$helpers = $this->get("app.helpers");
		$em = $this->getDoctrine()->getManager();
		
		$json = $request->get("json", null);
		$params = json_decode($json);
		$hash = $request->get("auth", null);
		$authCheck = $helpers->authCheck($hash);
		//$authCheck = true;
		
		if ($authCheck) {
		
			if ($json != null) {

				$userId = (isset($params->user_id)) ? $params->user_id : null;
				$sportId = (isset($params->sport_id)) ? $params->sport_id : 1;
				$teamId = (isset($params->team_id)) ? $params->team_id : 0;			

				if ($userId != null && $userId > 0) {

				$user = $em->getRepository("BackendBundle:User")->findOneBy(array(
					"id" => $userId
				));

				$isset_stats = $em->getRepository("BackendBundle:FotballPlStats")->findOneBy(array(
					"user" => $user	
				));				

					if (!isset($isset_stats)) {

						$sport = $em->getRepository("BackendBundle:Sport")->find($sportId);
						$team = $em->getRepository("BackendBundle:Team")->find($teamId);

						$stats = new FotballPlStats();
						$stats ->setUser($user);
						$stats ->setSport($sport);
						$stats ->setTeam($team);
						
						$stats->setAlternate(0);
						$stats->setAssessment(3);
						$stats->setAssists(0);
						$stats->setPosition("");
						$stats->setGames(0);
						$stats->setTitular(0);
						$stats->setNotPlay(0);
						$stats->setNotGame(0);
						$stats->setMinutes(0);
						$stats->setGoals(0);
						$stats->setPasses(0);
						$stats->setYellowT(0);
						$stats->setRedT(0);						

						$em ->persist($stats);
						$em ->flush();

						$statsNew = $em->getRepository("BackendBundle:FotballPlStats")
								->find($stats->getId());

						$data = array(
							"status" => "success",
							"code" => 400,
							"msg" => "stats created",
							"data" => $statsNew
						);

					} else {
						$data = array(
							"status" => "error",
							"code" => 400,
							"msg" => "stats already exist"
						);
					}

				} else {
					$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "user invalid"
					);
				}

			} else {
				$data = array(
					"status" => "error",
					"code" => 400,
					"msg" => "json failed"
				);
			}
		
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "authorization failed"
			);
		}
		
		return $helpers -> json($data);
	}
	
	public function editAction(Request $request, $id=null) {
		
		// helpers y auth
		$helpers = $this->get("app.helpers");
		$em = $this->getDoctrine()->getManager();
		$hash = $request->get('auth', null);
		$authCheck = $helpers->authCheck($hash);
		
		if ($authCheck) {
		
			// Obtener json
			$json = $request->get("json", null);
			$params = json_decode($json);

			if ($json != null) {
				
				// Obtener parametros
				$userId = (isset($params->user_id)) ? $params->user_id : null;
				$sportId = (isset($params->sport_id)) ? $params->sport_id : 1;
				$teamId = (isset($params->team_id)) ? $params->team_id : null;
				$position = (isset($params->position)) ? $params->position : null;
				$games = (isset($params->games)) ? $params->games : null;
				$titular = (isset($params->titular)) ? $params->titular : null;
				$alternate = (isset($params->alternate)) ? $params->alternate : null;
				$not_play = (isset($params->not_play)) ? $params->not_play : null;
				$not_game = (isset($params->not_game)) ? $params->not_game : null;
				$minutes = (isset($params->minutes)) ? $params->minutes : null;
				$goals = (isset($params->goals)) ? $params->goals : null;
				$passes = (isset($params->passes)) ? $params->passes : null;
				$yellow_t = (isset($params->yellow_t)) ? $params->yellow_t : null;
				$red_t = (isset($params->red_t)) ? $params->red_t : null;
				$assessment = (isset($params->assessment)) ? $params->assessment : null;
				$assists = (isset($params->assists)) ? $params->assists : null;
				
				// Buscar al usuario exsistente
				$user = $em->getRepository("BackendBundle:User")->find($id);

				if ($user) {

					$stats = $em->getRepository("BackendBundle:FotballPlStats")->findOneBy(array(
						"user" => $user
					));
					
					if ($stats) {

						$stats->setAlternate($alternate);
						$stats->setAssessment($assessment);
						$stats->setAssists($assists);
						$stats->setPosition($position);
						$stats->setGames($games);
						$stats->setTitular($titular);
						$stats->setNotPlay($not_play);
						$stats->setNotGame($not_game);
						$stats->setMinutes($minutes);
						$stats->setGoals($goals);
						$stats->setPasses($passes);
						$stats->setYellowT($yellow_t);
						$stats->setRedT($red_t);
						
						$em->persist($stats);
						$em->flush();
						
						$data = array(
							"status" => "success",
							"code" => 200,
							"msg" => "stats updated",
							"data" => $stats
						);
						
					} else {
						$data = array(
							"status" => "error",
							"code" => 400,
							"msg" => "stats dont found"
						);
					}

				} else {
					$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "user not found"
					);
				}

			} else {
				$data = array(
					"status" => "error",
					"code" => 400,
					"msg" => "json not valid"
				);
			}
			
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "authorization not valid"
			);
		}
		
		return $helpers -> json($data);
	}
	
	public function detailAction(Request $request, $id=null ) {
		
		$helpers = $this->get('app.helpers');
		$hash = $request->get('auth', null);
		$authcheck = $helpers->authCheck($hash);
		
		if ($authcheck) {
			
			$em = $this->getDoctrine()->getManager();
			$user = $em->getRepository("BackendBundle:User")->find($id);
			$stats = $em->getRepository("BackendBundle:FotballPlStats")->findOneBy(array(
				"user" => $user
			));
			
			if ($stats) {
				$data = array(
					"status" => "success",
					"code" => "200",
					"data" => $stats
				);
				
			} else {
				$data = array(
					"status" => "error",
					"code" => "400",
					"msg" => "stats not found"
				);
			}
			
		} else {
			$data = array(
				"status" => "error",
				"code" => "400",
				"msg" => "authorization not valid"
			);
		}
		
		return $helpers ->json($data);
	}
	
}