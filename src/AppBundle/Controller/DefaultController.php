<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use BackendBundle\Entity\Club;

class DefaultController extends Controller {

	/**
	 * @Route("/", name="homepage")
	 */
	public function indexAction(Request $request) {
		// replace this example code with whatever you need
		return $this->render('default/index.html.twig', array(
					'base_dir' => realpath($this->container->getParameter('kernel.root_dir') . '/..'),
		));
	}

	public function loginAction(Request $request) {
		$helpers = $this->get("app.helpers");
		$jwt_auth = $this->get("app.jwt_auth");

		//Recibir json por POST
		$json = $request->get("json", null);

		if ($json != null) {
			$params = json_decode($json);

			$email = (isset($params->email)) ? $params->email : null;
			$password = (isset($params->password)) ? $params->password : null;
			$getHash = (isset($params->gethash)) ? $params->gethash : null;

			$emailConstraint = new Assert\Email();
			$emailConstraint->message = "This email is not valid !";
			$validate_email = $this->get("validator")->validate($email, $emailConstraint);
			
			//Cifrar password
			$pwd = hash('sha256', $password);

			if (count($validate_email) == 0 && $password != null) {

				if ($getHash == null || $getHash == "false") {
					$signup = $jwt_auth->signup($email, $pwd);					
				} else{
					$signup = $jwt_auth->signup($email, $pwd, true);
				}
				return new \Symfony\Component\HttpFoundation\JsonResponse($signup);
			} else {
				return $helpers->json(array(
							"status" => "error",
							"data" => "json not valid"
				));
			}
		} else {
			return $helpers->json(array(
						"status" => "error",
						"data" => "Send json with POST !"
			));
		}
	}

	public function pruebasAction(Request $request) {
		$helpers = $this->get("app.helpers");

//		$em = $this->getDoctrine()->getManager();
//		$users = $em->getRepository('BackendBundle:User')->findAll();
		
		$hash = $request -> get("auth", null);
		$check = $helpers -> authCheck($hash, true);
		
		var_dump($check);
		die();

//		return $helpers->json($users);
	}

}
