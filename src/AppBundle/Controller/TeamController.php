<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;
use BackendBundle\Entity\User;
use BackendBundle\Entity\Sport;
use BackendBundle\Entity\Team;

class TeamController extends Controller {
	
	public function newAction(Request $request){
		
		$helpers = $this -> get('app.helpers');
		$hash = $request -> get('auth', null);
		$authcheck = $helpers->authCheck($hash, $roles='user');
		
		if ($authcheck){
			
			$identity = $helpers->authCheck($hash, true);
			$json = $request->get('json');
			
			if ($json != null){
				
				$em = $this->getDoctrine()->getManager();
				$params = json_decode($json);				
				$createdAt = new \DateTime("now");
				
				$name = (isset($params->name)) ? $params->name : null;
				$description = (isset($params->description)) ? $params->description : null;
				$image = (isset($params->image)) ? $params->image : null;
				$status = (isset($params->status)) ? $params->status : "private";
				$sportId = (isset($params->sportId)) ? $params->sportId : null;
				$clubId = (isset($params->clubId)) ? $params->clubId : null;
				
				$creator = $em->getRepository("BackendBundle:User")->findOneBy(array(
						"id" => $identity->sub
					));
				
				if ($name != null && $sportId != null && $clubId != null){
					
					$sport = $em->getRepository("BackendBundle:Sport")->findOneBy(array(
							"id" => $sportId
						));
					
					$club = $em->getRepository("BackendBundle:Club")->findOneBy(array(
							"id" => $clubId
						));
										
					$nameCheck = $em->getRepository("BackendBundle:Team")->findOneBy(array(
						"name" => $name,
						"sport" => $sport,
						"club" => $club
					));					
					
					if (count($nameCheck) == 0){												
						
						$team = new Team();
						$team ->setName($name);
						$team ->setDescription($description);
						$team ->setImage($image);
						$team ->setCreatedAt($createdAt);
						$team ->setStatus($status);
						$team ->setSport($sport);
						$team ->setCreator($creator);
						$team ->setClub($club);
						
						$em -> persist($team);
						$em -> flush();
						
						$data = array(
							"status" => "success",
							"code" => 200,
							"msg" => "Team created",
							"team" => $team
						);
						
					} else {
						$data = array(
							"status" => "error",
							"code" => 400,
							"msg" => "Team not created, name is already exists"
						);
					}
					
				} else {
					$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "Team not created, requiered data failed"
					);
				}								
				
			} else {
				$data = array(
					"status" => "error",
					"code" => 400,
					"msg" => "Team not created, json failed"
				);
			}
			
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "Team not created, authorization failed"
			);
		}
		
		return $helpers->json($data);
	}
	
	public function editAction(Request $request, $id=null) {
		
		// helpers y auth
		$helpers = $this->get("app.helpers");
		$json = $request->get("json", null);
		$params = json_decode($json);
		$hash = $request->get("auth", null);
		$authCheck = $helpers->authCheck($hash);		
		
		if ($authCheck) {						
			
			if ($json != null){
				
				$updatedAt = new \Datetime	("now");

				$name = (isset($params->name)) ? $params->name : null;				
				$description = (isset($params->description)) ? $params->description : null;
				$image = (isset($params->image)) ? $params->image : null;				
				$status = (isset($params->status)) ? $params->status : null;

				$em = $this->getDoctrine()->getManager();
				
				$team = $em->getRepository("BackendBundle:Team")->find($id);
				
				if ($team) {
					
					//$team->setUpdatedAt($updatedAt);
					$team->setName($name);
					$team->setDescription($description);
					$team->setImage($image);
					$team->setStatus($status);
					
					$em -> persist($team);
					$em -> flush();

					$data["status"] = "success";
					$data["code"]   = 200;	
					$data["msg"]    = "Team updated";
					$data["data"]   = $team;					
					
				} else {
					$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "team not found"
					);
				}
				
			} else {
				$data = array(
					"status" => "error",
					"code" => 400,
					"msg" => "json not found"
				);
			}
			
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "authorization failed"
			);
		}
		
		return $helpers->json($data);
	}
	
	public function detailAction(Request $request, $id=null) {
		
		$helpers = $this->get("app.helpers");
		$hash = $request->get("auth", null);
		$authCheck = $helpers->authCheck($hash);		
		
		if ($authCheck) {
			
			$em = $this->getDoctrine()->getManager();
			$team = $em->getRepository("BackendBundle:Team")->find($id);
			
			$team = ($team->getStatus()=="deleted") ? null : $team;
			
			if ($team) {
				
				$data = array(
					"status" => "success",
					"code" => "200",
					"data" => $team
				);				
				
			} else {
				$data = array(
					"status" => "error",
					"code" => 400,
					"msg" => "Team not found"
				);
			}
			
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "authorization failed"
			);
		}
		
		return $helpers->json($data);
	}
	
	public function listAction(Request $request) {
		
		$helpers = $this -> get("app.helpers");
		$hash = $request->get("auth", null);
		$authCheck = $helpers->authCheck($hash);
		$em = $this->getDoctrine()->getManager();				
		
		if ($authCheck) {
			
			$sport = $request->query->get('sport');
			$club = $request->query->get('club');
			
//			$sport = $em->getRepository("BackendBundle:Sport")->findOneBy(array(
//				"id" => $sportId
//			));
			
			if (!empty($sport) && !empty($club)) {

				$teams = $em->getRepository("BackendBundle:Team")->findBy(array(
					"sport" => $sport,
					"club" => $club,
					"status" => "active"
				), array('id'=>'desc'));

				if (count($teams) >= 1) {
					$data = array(
						"status" => "success",
						"code" => 200,
						"data" => $teams
					);

				} else {
					$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "dont exist teams in this sport"
					);
				}
				
			} else {
				$data = array(
					"status" => "error",
					"code" => 400,
					"msg" => "data requiered"
				);
			}
			
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "auth error"
			);
		}
		
		return $helpers -> json($data);
	}
	
	public function deleteAction(Request $request, $id=null) {
		
		$helpers = $this -> get("app.helpers");
		$hash = $request->get("auth", null);
		$authCheck = $helpers->authCheck($hash);
		
		if ($authCheck) {
			
			$identity = $helpers->authCheck($hash, true);
			$userId = (isset($identity->sub)) ? $identity->sub : null;
			
			$em = $this->getDoctrine()->getManager();
			$team = $em->getRepository("BackendBundle:Team")->findOneBy(array(
				"id" => $id
			));
			
			if(is_object($team) && $userId != null) {
				
				if(isset($identity->sub) && (
						$userId == $team->getCreator()->getId() || 
						$userId == $team->getSport()->getUser()->getId() )) {
					
					$team -> setStatus("deleted");
					$em->persist($team);
//					$em -> remove($team);
					$em -> flush();
					
					$data = array(
						"status" => "success",
						"code" => 200,
						"msg" => "Team deleted"
					);
					
				} else {
					$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "You are not the owner"
					);
				}
				
			} else {
				$data = array(
					"status" => "error",
					"code" => 400,
					"msg" => "Traing not found"
				);
			}
			
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "Authentication not valid"
			);
		}
		
		return $helpers -> json($data);
		
	}
	
	public function checkNameAction(Request $request) {
		$helpers = $this -> get("app.helpers");
		$name = $request->query->get('name');
		$sport = $request->query->get('sport');
		$club = $request->query->get('club');
		
		if (!empty($name) && !empty($sport) && !empty($club)) {
			
			$em = $this->getDoctrine()->getManager();
			$team = $em->getRepository("BackendBundle:Team")->findOneBy(array(
				"name" => $name,
				"sport" => $sport,
				"club" => $club
			));
			
			if (empty($team)) {				
				$data = array(
					"status" => "success",
					"code" => 200,
					"msg" => "Name is valid"
				);
				
			} else {
				$data = array(
					"status" => "error",
					"code" => 409,
					"msg" => "Name is already used"
				);
			}
			
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "Requiered data failed"
			);
		}			
		
		return $helpers -> json($data);
	}
		
	
}