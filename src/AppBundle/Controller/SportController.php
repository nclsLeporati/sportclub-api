<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;
use BackendBundle\Entity\User;
use BackendBundle\Entity\Sport;

class SportController extends Controller {
	
	public function editAction(Request $request, $id = null) {
		
		$sportId = $id;
		$helpers = $this ->get('app.helpers');
		$hash = $request->get("auth", null);
		$authCheck = $helpers->authCheck($hash);
		
		if ($authCheck == true) {
			
			$identity = $helpers->authCheck($hash, true);			
			$json = $request -> get("json", null);	
			
			if ($json != null) {	
				
				$params = json_decode($json);

				$updatedAt = new \DateTime("now");
				$image = null;	
				
				$userId = ($identity->sub != null) ? $identity->sub : null;
				$name = (isset($params->name)) ? $params->name : null;
				$description = (isset($params->description)) ? $params->description : null;
				$status = (isset($params->status)) ? $params->status : null;
				
				if ($userId != null && $name != null && $description != null) {
				
					$em = $this->getDoctrine()->getManager();
					$sport = $em ->getRepository("BackendBundle:Sport")->findOneBy(array(
						"id" => $sportId
					));										
					
					if (isset($identity->sub) && $identity->sub == $sport->getUser()->getId()) {
						
						$sport -> setName($name);
						$sport -> setDescription($description);
						$sport -> setStatus($status);
						$sport -> setUpdatedAt($updatedAt);

						$em -> persist($sport);
						$em -> flush();											

						$data = array(
							"status" => "success",
							"code" => 200,
							"msg" => "Sport update success"
						);
					
					} else {
						$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "Sport not updated, user is not owner"
					);
					}
					
				} else {
					$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "Sport not updated, params failed	"
					);
				}
			
			} else {
				$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "Sport not updated, json failed"
					);
			}
					
			
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "Authorization not valid"
			);
		}
		
		return $helpers -> json($data);
	}
	
		public function newAction(Request $request) {
		
		$helpers = $this ->get('app.helpers');
		$hash = $request->get("auth", null);
		$authCheck = $helpers->authCheck($hash);
		
		if ($authCheck == true) {
			
			$identity = $helpers->authCheck($hash, true);			
			$json = $request -> get("json", null);	
			
			if ($json != null) {	
				
				$params = json_decode($json);

				$createdAt = new \Datetime("now");
				$updatedAt = new \DateTime("now");
				$image = null;	
				
				$userId = ($identity->sub != null) ? $identity->sub : null;
				$name = (isset($params->name)) ? $params->name : null;
				$description = (isset($params->description)) ? $params->description : null;
				$status = (isset($params->status)) ? $params->status : null;
				
				if ($userId != null && $name != null) {
					
					$em = $this->getDoctrine()->getManager();
					$user = $em->getRepository("BackendBundle:User")->findOneBy(array(
						"id" => $userId
					));
				
					$sport = new Sport();
					$sport -> setUser($user);
					$sport -> setName($name);
					$sport -> setDescription($description);
					$sport -> setStatus($status);
					$sport -> setCreatedAt($updatedAt);
					$sport -> setUpdatedAt($updatedAt);
					$sport ->setImage("uploads/sports/assets/icon_" . $name) . ".png";
					
					$em -> persist($sport);
					$em -> flush();
					
					$sport = $em->getRepository("BackendBundle:sport")->findOneBy(array(
						"user"      => $user,
						"name"      => $name,
						"status"    => $status,
						"createdAt" => $createdAt
					));
					
					$data = array(
						"status" => "success",
						"code" => 200,
						"sport" => $sport
					);
					
				} else {
					$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "Sport not created"
					);
				}
			
			} else {
				$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "Sport not created, json failed"
					);
			}
					
			
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "Authorization not valid"
			);
		}
		
		return $helpers -> json($data);
	}
	
	public function uploadAction(Request $request, $id) {
		
		$helpers = $this ->get("app.helpers");
		$hash = $request->get("auth", null);
		$authCheck = $helpers->authCheck($hash);
		
		if ($authCheck == true) {
			
			$identity = $helpers->authCheck($hash, true);			
			
			$sportId = $id;
			$em = $this->getDoctrine()->getManager();
			$sport = $em->getRepository("BackendBundle:Sport")->findOneBy(array(
				"id" => $sportId
			));			
			
			if ($sportId != null && isset($identity->sub) && $identity->sub == $sport->getUser()->getId()) {
				
				$file_image = $request->files->get('image');
				$file_video = $request->files->get('video');
				
				if ($file_image != null && !empty($file_image)) {
					$ext =	$file_image->guessExtension();
					
					if ($ext == "jpeg" || $ext == "jpg" || $ext == "png" || $ext == "gif") {
						$file_name = "image_".time().".".$ext;
						$file_image->move("uploads/sports/".$sport->getName()."/images", $file_name);

						$sport -> setImage($file_name);

						$em->persist($sport);
						$em->flush();

						$data = array(
							"status" => "success",
							"code" => 200,
							"msg" => "Image file uploaded"
						);
					} else {
						$data = array(
							"status" => "error",
							"code" => 400,
							"msg" => "Image format not valid"
						);
					}
					
				} else {
					if ($file_video != null && !empty($file_video)) {
					$ext =	$file_video->guessExtension();
					
					if ($ext == "mp4" || $ext == "avi" || $ext == "flv") {
							$file_name = "video_".time().".".$ext;
							$file_video->move("uploads/sports/".$sport->getName()."/videos", $file_name);

							//$sport -> setImage($file_name);

							//$em->persist($sport);
							//$em->flush();

							$data = array(
								"status" => "success",
								"code" => 200,
								"msg" => "Video file uploaded"
							);
						} else {
							$data = array(
								"status" => "error",
								"code" => 400,
								"msg" => "Video format not valid"
							);
						}
					
					}
									
				} 
				
			} else {
				$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "You are not admin for this sport",
				"auth" => $authCheck,
				"sport_id" => $sportId,
				"user_id" => $sport->getUser()->getId()
			);
			}
			
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "Authorization not valid",
				"auth" => $authCheck,
				"hash" => $request->get('auth')
			);
		}
		
		return $helpers->json($data);
		
	}
	
	public function sportsAction(Request $request) {
		$helpers = $this->get("app.helpers");
		
		$em = $this->getDoctrine()->getManager();
		
		$sql = "SELECT s FROM BackendBundle:Sport s ORDER BY s.id DESC";
		$query = $em->createQuery($sql);
		
		$page = $request->query->getInt("page", 1);
		$paginator = $this->get("knp_paginator");
		$items_per_page = 6;
		
		$pagination = $paginator->paginate($query, $page, $items_per_page);
		$total_item_count = $pagination->getTotalItemCount();
		
		$data = array(
			"status" => "success",
			"total_items_count" => $total_item_count,
			"page" => $page,
			"items_per_page" => $items_per_page,
			"total_pages" => ceil($total_item_count/$items_per_page),
			"data" => $pagination
		);
		
		return $helpers->json($data);
	}
	
	public function lastsSportsAction(Request $request) {
		$helpers = $this ->get("app.helpers");
		$em = $this->getDoctrine()->getManager();
		
		$dql = "SELECT s FROM BackendBundle:Sport s ORDER BY s.createdAt DESC";
		$query = $em->createQuery($dql)->setMaxResults(5);
		$sports = $query->getResult();
		
		$data = array(
			"status" => "success",
			"data" => $sports
		);
		
		return $helpers->json($data);
	}
	
	public function sportAction(Request $request, $id=null) {
		$helpers = $this->get("app.helpers");
		$em = $this->getDoctrine()->getManager();
		
		$sport = $em->getRepository("BackendBundle:Sport")->findOneBy(array(
			"id" => $id
		));
		
		if ($sport) {
			$data = array();
			$data["status"] = "success";
			$data["code"] = 200;
			$data["data"] = $sport;
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "Sport dont exist"
			);
		}
		
		return $helpers->json($data);
	} 
	
	public function searchAction(Request $request, $search=null) {
		$helpers = $this->get("app.helpers");		
		$em = $this->getDoctrine()->getManager();
		
		if ($search != null) {
			$sql =  "SELECT s FROM BackendBundle:Sport s ".
					"WHERE s.name LIKE :search OR ".
					"s.description LIKE :search ORDER BY s.id DESC";
			$query = $em->createQuery($sql)->setParameter("search", "%$search%");
		} else {
			$sql = "SELECT s FROM BackendBundle:Sport s ORDER BY s.id DESC";
			$query = $em->createQuery($sql);
		}		
		
		$page = $request->query->getInt("page", 1);
		$paginator = $this->get("knp_paginator");
		$items_per_page = 6;
		
		$pagination = $paginator->paginate($query, $page, $items_per_page);
		$total_item_count = $pagination->getTotalItemCount();
		
		$data = array(
			"status" => "success",
			"total_items_count" => $total_item_count,
			"page" => $page,
			"items_per_page" => $items_per_page,
			"total_pages" => ceil($total_item_count/$items_per_page),
			"data" => $pagination
		);
		
		return $helpers->json($data);
	}

	public function creatorAction(Request $request, $id=null) {
		$helpers = $this->get("app.helpers");
		
		$em = $this->getDoctrine()->getManager();
		
		$user = $em->getRepository("BackendBundle:User")->findOneBy(array(
			"id" => $id
		));
		
		$sql = "SELECT s FROM BackendBundle:Sport s WHERE s.user = $id ORDER BY s.id DESC";
		$query = $em->createQuery($sql);
		
		$page = $request->query->getInt("page", 1);
		$paginator = $this->get("knp_paginator");
		$items_per_page = 6;
		
		$pagination = $paginator->paginate($query, $page, $items_per_page);
		$total_item_count = $pagination->getTotalItemCount();
		
		if (count($user) == 1) {
			$data = array(
				"status" => "success",
				"total_items_count" => $total_item_count,
				"page" => $page,
				"items_per_page" => $items_per_page,
				"total_pages" => ceil($total_item_count/$items_per_page),				
			);
			
			$data["data"]["sports"] = $pagination;
			$data["data"]["user"] = $user;
			
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "User dont exist"
			);
		}
		
		return $helpers->json($data);
	}
	
	public function detailAction(Request $request, $id=null) {
		
		$helpers = $this->get("app.helpers");
		$em = $this->getDoctrine()->getManager();
		
		$sport = $em->getRepository("BackendBundle:Sport")->findOneBy(array(
			"id" => $id
		));
		
		if ($sport) {
			$data = array();
			$data["status"] = "success";
			$data["code"] = 200;
			$data["data"] = $sport;
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "Sport dont exist"
			);
		}
		
		return $helpers->json($data);		
	}
}