<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use BackendBundle\Entity\User;
use BackendBundle\Entity\Media;

class MediaController extends Controller {
	
	public function newAction(Request $request, $id=null) {
		
		$helpers = $this->get("app.helpers");
		$json = $request->get("json", null);
		$params = json_decode($json);
		
		if ($json != null) {
			
			$uploadAt = new \Datetime	("now");
			$status = "public";
			$restriction = "any";
			
			
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "Image not created"
			);
		}
	}

}