<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;
use BackendBundle\Entity\User;
use BackendBundle\Entity\FotballPlStats;
use AppBundle\Controller\PlayerStats;
use Doctrine\Common\Collections\ArrayCollection;
use BackendBundle\Repository\UserRepository;

class UserController extends Controller {
	
	public function registerAction(Request $request) {
		
		// helpers y json
		$helpers = $this->get("app.helpers");		
		$json = $request->get("json", null);
		$params = json_decode($json);		
		
		if ($json != null) {
			
			// Datos de User
			$createdAt = new \Datetime	("now");
			$image = "user-male.png";
			
			$email = (isset($params->email)) ? $params->email : null;
			$name = (isset($params->name) && ctype_alpha($params->name)) ? $params->name : null;
			$run = (isset($params->run)) ? $params->run : null;
			$lastname = (isset($params->lastname) && ctype_alpha($params->lastname)) ? $params->lastname : null;
			$password = (isset($params->password)) ? $params->password : null;
			$role = (isset($params->role)) ? $params->role : null;
			$clubId = (isset($params->club)) ? $params->club : null;
			$cellnumber = (isset($params->cellnumber)) ? $params->cellnumber : null;
			$birthDate = (isset($params->birthDate)&&$params->birthDate!="") ? $params->birthDate : null;
			$sportId = (isset($params->sport)) ? $params->sport : null;
			$status = (isset($params->status)) ? $params->status : "inactive";
			$teamId = (isset($params->teams)) ? $params->teams : null;
						
			if ($sportId != null && $sportId != ""){
				$em = $this->getDoctrine()->getManager();
				$sport = $em ->getRepository("BackendBundle:Sport")->findOneBy(array(
					"id" => $sportId
				));
			} else {
				$sport = null;
			}
			
			$club = null;
			if ($clubId != null && $clubId != ""){
				$em = $this->getDoctrine()->getManager();
				$club = $em->getRepository("BackendBundle:Club")->find($clubId);
			}				
			
			// Comprobar email
			$emailConstraint = new Assert\Email();
			$emailConstraint->message = "This email is not valid !";
			$validate_email = $this->get("validator")->validate($email, $emailConstraint);
			
			if ($email != null && count($validate_email) == 0 && 
					$password != null && $name != null && $lastname != null &&
					$role != null && $role != "") {
				
				// Crear usuario
				$user = new User();
				$user->setCreatedAt($createdAt);
				$user->setImage($image);
				$user->setRole($role);
				$user->setCellnumber($cellnumber);
				$user->setBirthDate($birthDate);
				$user->setStatus($status);
				$user->setEmail($email);
				$user->setName($name);
				$user->setRun($run);
				$user->setLastname($lastname);
				$user->setClub($club);
				if ($sport!=null)
					$user->addSport($sport);
				
				// Cifrado de contraseña				
				$pwd = hash('sha256', $password);
				$user->setPassword($pwd);				
				
				// Comprobar si email esta en uso
				$em = $this->getDoctrine()->getManager();
				$isset_user = $em->getRepository("BackendBundle:User")->findBy(
						array(
							"email" => $email
						));
				
				if (count($isset_user) == 0) {					
					
					// Guardar y terminar
					$em -> persist($user);
					$em -> flush();
					
					$data["status"] = "success";
					$data["code"]   = 200;	
					$data["msg"]    = "New User created";
					$data["data"]	= $user;					
					
				} else {
					$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "duplicated email"
					);
				}
				
			} else {			
				$data = array(
					"status" => "error",
					"code" => 400,
					"msg" => "validation failed",
					"sport" => $sport,
					"club" => $club,
					"params" => $params
				);
			}
			
		} else {			
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "json null"
			);
		}
		
		return $helpers -> json($data);
	}
	
	public function newAction(Request $request) {
		
		// helpers y json
		$helpers = $this->get("app.helpers");		
		$json = $request->get("json", null);
		$params = json_decode($json);
		$hash = $request->get("auth", null);
		$authCheck = $helpers->authCheck($hash);
		
		if ($json != null) {
			
			if ($authCheck) {
			
				// Datos de User
				$createdAt = new \Datetime	("now");
				$image = "user-male.png";

				$email = (isset($params->email)) ? $params->email : null;
				$name = (isset($params->name) && ctype_alpha($params->name)) ? $params->name : null;
				$run = (isset($params->run)) ? $params->run : null;
				$lastname = (isset($params->lastname) && ctype_alpha($params->lastname)) ? $params->lastname : null;
				$password = (isset($params->password)) ? $params->password : null;
				$role = (isset($params->role)) ? $params->role : null;
				$clubId = (isset($params->club)) ? $params->club : null;
				$cellnumber = (isset($params->cellnumber)) ? $params->cellnumber : null;
				$birthDate = (isset($params->birthDate)&&$params->birthDate!="") ? $params->birthDate : null;
				$sportId = (isset($params->sport)) ? $params->sport : null;
				$teamId = (isset($params->teams)) ? $params->teams : null;
				$status = (isset($params->status)) ? $params->status : "inactive";

				if (!empty($sportId)){
					$em = $this->getDoctrine()->getManager();
					$sport = $em ->getRepository("BackendBundle:Sport")->findOneBy(array(
						"id" => $sportId
					));
				} else {
					$sport = null;
				}
				
				if (!empty($clubId)){
					$em = $this->getDoctrine()->getManager();
					$club = $em->getRepository("BackendBundle:Club")->find($clubId);
				} else {
					$club = null;
				}
				
				if (!empty($teamId)){
					$em = $this->getDoctrine()->getManager();
					$team = $em->getRepository("BackendBundle:Team")->find($teamId);
				} else {
					$team = null;
				}

				// Comprobar email
				$emailConstraint = new Assert\Email();
				$emailConstraint->message = "This email is not valid !";
				$validate_email = $this->get("validator")->validate($email, $emailConstraint);

				if ($email != null && count($validate_email) == 0 && 
						$password != null && $name != null && $lastname != null &&
						$role != null && $role != "") {

					// Crear usuario
					$user = new User();
					$user->setCreatedAt($createdAt);
					$user->setImage($image);
					$user->setRole($role);
					$user->setCellnumber($cellnumber);
					$user->setBirthDate($birthDate);
					$user->setStatus($status);
					$user->setEmail($email);
					$user->setName($name);
					$user->setRun($run);
					$user->setLastname($lastname);
					$user->setClub($club);
					if ($sport!=null) $user->addSport($sport);
					if ($team!=null) $user->addTeam($team);

					// Cifrado de contraseña				
					$pwd = hash('sha256', $password);
					$user->setPassword($pwd);				

					// Comprobar si email esta en uso
					$em = $this->getDoctrine()->getManager();
					$isset_user = $em->getRepository("BackendBundle:User")->findBy(
							array(
								"email" => $email
							));

					// Guardar y terminar
					if (count($isset_user) == 0) {					
						
						$em -> persist($user);
						$em -> flush();

						$data["status"] = "success";
						$data["code"]   = 200;	
						$data["msg"]    = "New User created";
						$data["data"]	= $user;					

					} else {
						$data = array(
							"status" => "error",
							"code" => 400,
							"msg" => "duplicated email"
						);
					}

				} else {			
					$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "validation failed",
						"sport" => $sport,
						"club" => $club,
						"params" => $params
					);
				}
			
			} else {			
				$data = array(
					"status" => "error",
					"code" => 400,
					"msg" => "auth failed",
					"auth" => $authCheck
				);
			}
			
		} else {			
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "json null"
			);
		}
		
		return $helpers -> json($data);
	}
	
	public function editAction(Request $request, $id=null) {
		
		// helpers y auth
		$helpers = $this->get("app.helpers");
		$id = $request->get("id");			
		$json = $request->get("json", null);
		$params = json_decode($json);		
		$hash = $request->get("auth", null);
		$authCheck = $helpers->authCheck($hash);		
		
		if ($authCheck) {
			
			// Obtenemos la identidad
			$em = $this->getDoctrine()->getManager();
			$identity = $helpers->authCheck($hash, true);					
			
			if ($id == 0) {
				$user = $em->getRepository("BackendBundle:User")->find($identity->getSub());
			} else {
				$user = $em->getRepository("BackendBundle:User")->find($id);
			}
			
			// Obtenemos id y json
			$id = $user->getId();
			$email_validate = false;

			if ($json != null) {
				
				$updated_at = new \Datetime("now");
				
				// Obtenemos los parametros
				$role = (isset($params->role)) ? $params->role : null;
				$name = (isset($params->name) && ctype_alpha($params->name)) ? $params->name : null;				
				$run = (isset($params->run)) ? $params->run : null;
				$lastname = (isset($params->lastname) && ctype_alpha($params->lastname)) ? $params->lastname : null;
				$password = (isset($params->password)) ? $params->password : null;
				$email = (isset($params->email)) ? $params->email : null;
				$cellnumber = (isset($params->cellnumber)) ? $params->cellnumber : null;
				$birthDate = (isset($params->birthDate)) ? $params->birthDate : null;
				$origin = (isset($params->origin)) ? $params->origin : null;
				$nacionality = (isset($params->nacionality)) ? $params->nacionality : null;
				$originClub = (isset($params->originClub)) ? $params->originClub : null;				
				$status = (isset($params->status)) ? $params->status : null;
				$clubId = (isset($params->club->id)) ? $params->club->id : null;
				
//				if (isset($params->teams)) {
//					foreach ($params->teams as $t) {
//						$query = $em->getRepository("BackendBundle:Team")->createQueryBuilder('t')
//								->where('t.id = :id')
//								->andWhere(':team MEMBER OF t.users')
//								->setParameters(array(
//									'id' => $t->id,
//									'team' => $t
//									))
//								->getQuery();
//						$isset_team = $query->getResult();
//						
//						if (!$isset_team) {
//							$teams[] = $em->getRepository("BackendBundle:Team")->find($t->id);
//						}						
//					}
//				} else {
//					$teams = null;
//				}
				
//				if (isset($params->teams)) {
//					foreach ($params->teams as $t) {						
//						$team = $em->getRepository("BackendBundle:Team")->findById($t);
//						if ($team) {
//							$teams[] = $team;
//						}
//					}
//				} else {
//					$teams = null;
//				}				
				
				$club = $em->getRepository("BackendBundle:Club")->find($clubId);

				
				// Validamos el email
				$emailConstraint = new Assert\Email();
				$emailConstraint->message = "This email is not valid !";
				$validate_email = $this->get("validator")->validate($email, $emailConstraint);

				if (count($validate_email) == 0) {
					
					// Actualizamos datos
					//$user->setImage($image);
					$user->setRole($role);
					$user->setCellnumber($cellnumber);
					$user->setBirthDate($birthDate);
					$user->setNacionality($nacionality);
					$user->setOrigin($origin);
					$user->setOriginClub($originClub);
					$user->setStatus($status);				
					$user->setName($name);
					$user->setLastname($lastname);
					$user->setUpdatedAt($updated_at);
					$user->setClub($club);
					$user->setRun($run);
//					if ($teams) {
//						foreach ($teams as $t) {
//							$user->addTeam($t);
//						}												
//					}					
					
					// Se comprueba que se cambio la contraseña
					if ($password != null || !empty($password)) {
						//cifrado de contraseña
						$pwd = hash('sha256', $password);
						$user->setPassword($pwd);
					}
					
					// Buscamos y comprobamos el correo
//					$em = $this->getDoctrine()->getManager();
//					$old_user = $em->getRepository("BackendBundle:User")->findOneBy(
//							array(
//								"id" => $id
//							));
//					$old_email = (count($old_user) == 0) ? null : $old_user->getEmail();					
//					
//					$isset_email = null;
//					if ($email != $old_email) {
//						$isset_email = $em->getRepository("BackendBundle:User")->findOneBy(
//								array(
//									"email" => $email
//								));
//					}
					
					$isset_email = $em->getRepository("BackendBundle:User")->findOneBy(
						array(
							"email" => $email
						));
					$isset_email = ($isset_email === null) ? null : $isset_email->getEmail();
					
					$old_email = $em->getRepository("BackendBundle:User")->findOneById($id)->getEmail();					

					if ($isset_email === null || $isset_email === $old_email) {
						
						// Guardamos y terminamos
						$user->setEmail($email);
						$em -> persist($user);
						$em -> flush();

						$data["status"] = "success";
						$data["code"]   = 200;
						$data["msg"]    = "User updated";
						$data["data"]    = $user;

					} else {
						$data = array(
							"status" => "error",
							"code" => 409,
							"msg" => "duplicated email"
						);
					}
				
				} else {
					$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "json failed"
					);
				}
				
			} else {
				$data = array(
					"status" => "error",
					"code" => 400,
					"msg" => "json not found"
				);
			}
			
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "auth failed"
			);
		}
		
		return $helpers -> json($data);
	}	
	
	public function detailAction(Request $request, $id=null){
		
		// helpers y auth
		$helpers = $this->get('app.helpers');
		$hash = $request->get('auth', null);
		$authcheck = $helpers->authCheck($hash);
		
		if ($authcheck) {
			
			// Se busca al usuario
			$em = $this->getDoctrine()->getManager();
			$user = $em->getRepository("BackendBundle:User")->find($id);
			
			if ($user) {
				$data = array(
					"status" => "success",
					"code" => "200",
					"data" => $user
				);
				
			} else {
				$data = array(
					"status" => "error",
					"code" => "400",
					"msg" => "user not found"
				);
			}
			
		} else {
			$data = array(
				"status" => "error",
				"code" => "400",
				"msg" => "authorization not valid"
			);
		}
		
		return $helpers ->json($data);
	}
	
	public function listAction(Request $request) {
		
		// helpers y auth
		$helpers = $this -> get("app.helpers");
		$sportId = $request->get("sport", null);
		$clubId = $request->get("club", null);
		$teamId = $request->get("team", null);
		$hash = $request->get("auth", null);		
		$authCheck = $helpers->authCheck($hash);
		
		if ($authCheck) {
				
			$em = $this->getDoctrine()->getManager();
			$identity = $helpers->authCheck($hash, true);
			$params = array();
			
			// Se busca el deporte
			if (!empty($sportId)) {
				$sport = $em->getRepository("BackendBundle:Sport")->findOneById($sportId);
				(!empty($sport)) ? $params['sport'] = $sport : $params['sport'] = $sportId;
			}
			
			// Se busca el club
			if (!empty($clubId)) {
				$club = $em->getRepository("BackendBundle:Club")->findOneById($clubId);
				(!empty($club)) ? $params['club'] = $club : $params['club'] = $clubId;
			}
			
			// Se busca al equipo
			if (!empty($teamId)) {
				$team = $em->getRepository("BackendBundle:Team")->findOneById($teamId);
				(!empty($team)) ? $params['team'] = $team : $params['team'] = $teamId;
			}
			
			// Se establecen los demas parametros			
			$role = $request->query->get('role');
			
			if (!empty($role)) { $params['role'] = $role; }				
			
//			$users = $em->getRepository(User::class)->findUsersBySportRoleClub($sport, $role, $club);
			
			if (empty($params)) {
				$users = $em->getRepository(User::class)->findAll();
			} else {
//				$users = $em->getRepository(User::class)->findBy($params);
				$users = $em->getRepository(User::class)->findUsersBySport($params);
			}

			if (count($users) > 0) {
				$data = array(
					"status" => "success",
					"total" => count($users),
					"params" => $params,										
					"code" => 200,
					"data" => $users,									
				);

			} else {
				$data = array(
					"status" => "error",
					"code" => 400,
					"msg" => "no se encontraron usuarios",
					"params" => $params
				);
			}				
		
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "authorization invalid"
			);
		}
		
		return $helpers -> json($data);
	}
	
	public function uploadImageAction(Request $request) {
		
		// helpers y ath
		$helpers = $this->get("app.helpers");		
		$hash = $request->get("auth", null);
		$authCheck = $helpers->authCheck($hash);
		
		if ($authCheck == true) {
			
			// Se obtiene la identity y al usuario
			$identity = $helpers->authCheck($hash, true);
			
			$em = $this->getDoctrine()->getManager();
			$user = $em->getRepository("BackendBundle:User")->findOneBy(array(
				"id" => $identity->sub
			));
			
			// upload file
			$file = $request->files->get("image");
		
			if (!empty($file) && $file != null) {
				$ext = $file->guessExtension();
				if ($ext == "jpeg" || $ext == "jpg" || $ext == "png") {
					
					// Se guarda la imagen en el servidor
					$file_name = "user_image_".time().".".$ext;
					$file->move("uploads/users", $file_name);
					
					// Se setea la imagen al usuario y se termina
					$user->setImage($file_name);
					$em->persist($user);
					$em->flush();

					$data = array(
						"status" => "success",
						"code" => 200,
						"msg" => "Image for user upload success!",
						"data" => $file_name
					);
				} else {
					$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "File not valid"
					);
				}
			} else {
				$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "File not found"
					);
			}						
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "Image not upload"
			);
		}
		
		return $helpers->json($data);
	}
	
	public function addTeamAction(Request $request, $id = null) {
		$helpers = $this -> get("app.helpers");
		$id = $request->get("id");			
		$teamId = $request->get("team", null);		
		$hash = $request->get("auth", null);
		$authCheck = $helpers->authCheck($hash);
		
		if ($authCheck) {
			
			$em = $this->getDoctrine()->getManager();
			$user = $em->getRepository("BackendBundle:User")->findOneById($id);
			$team = $em->getRepository("BackendBundle:Team")->findOneById($teamId);
			

			$query = $em->getRepository("BackendBundle:User")->createQueryBuilder('u')
					->where('u.id = :id')
					->andWhere(':team MEMBER OF u.teams')
					->setParameters(array(
						'id' => $user->getId(),
						'team' => $team
						))
					->getQuery();
			$isfrom_team = $query->getResult();

			if (!$isfrom_team) {										
			
				// Guardamos y terminamos
				$user->addTeam($team);
				$em -> persist($user);
				$em -> flush();

				$data["status"] = "success";
				$data["code"]   = 200;
				$data["msg"]    = "Team added to user";
				$data["data"]	= $user;

			} else {
				$data = array(
					"status" => "error",
					"code" => 400,
					"msg" => "user is already on the team"
				);
			}				
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "auth failed"
			);
		}
		
		return $helpers->json($data);
	}
	
	public function removeTeamAction(Request $request, $id = null) {
		$helpers = $this -> get("app.helpers");
		$id = $request->get("id");			
		$teamId = $request->get("team", null);		
		$hash = $request->get("auth", null);
		$authCheck = $helpers->authCheck($hash);
		
		if ($authCheck) {
			
			$em = $this->getDoctrine()->getManager();
			$user = $em->getRepository("BackendBundle:User")->findOneById($id);
			$team = $em->getRepository("BackendBundle:Team")->findOneById($teamId);
			

			$query = $em->getRepository("BackendBundle:User")->createQueryBuilder('u')
					->where('u.id = :id')
					->andWhere(':team MEMBER OF u.teams')
					->setParameters(array(
						'id' => $user->getId(),
						'team' => $team
						))
					->getQuery();
			$isfrom_team = $query->getResult();

			if ($isfrom_team) {										
			
				// Guardamos y terminamos
				$user->removeTeam($team);
				$em -> persist($user);
				$em -> flush();

				$data["status"] = "success";
				$data["code"]   = 200;
				$data["msg"]    = "Team removed from user";
				$data["data"]	= $user;

			} else {
				$data = array(
					"status" => "error",
					"code" => 400,
					"msg" => "user is not on the team"
				);
			}				
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "auth failed"
			);
		}
		
		return $helpers->json($data);
	}

}