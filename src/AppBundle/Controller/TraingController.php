<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;
use BackendBundle\Entity\User;
use BackendBundle\Entity\Sport;
use BackendBundle\Entity\Traing;

class TraingController extends Controller {
	
	public function newAction(Request $request) {
		
		$helpers = $this -> get("app.helpers");
		$hash = $request->get("auth", null);
		$authCheck = $helpers->authCheck($hash);
		
		if ($authCheck) {
			
			$identity = $helpers->authCheck($hash,true);
			$json = $request->get("json", null);
			
			if ($json != null) {
				
				$params = json_decode($json);
				
				$createdAt = new \Datetime('now');
				$userId = (isset($identity->sub)) ? $identity->sub : null;
				$sportId = (isset($params->sport_id)) ? $params->sport_id : null;
				//$run = (isset($params->$run)) ? $params->$run : null;
				$clubId = (isset($params->clubId)) ? $params->clubId : null;
				$name = (isset($params->name)) ? $params->name : null;
				$description = (isset($params->description)) ? $params->description : null;;
				$typeId = 0;
				$body = (isset($params->body)) ? $params->body : null;
				$status = (isset($params->status)) ? $params->status : null;
				$birth_date = (isset($params->birth_date)) ? $params->birth_date : null;
				$origin = (isset($params->origin)) ? $params->origin : null;
				$nacionality = (isset($params->nacionality)) ? $params->nacionality : null;
				$origin_club = (isset($params->origin_club)) ? $params->origin_club : null;
				$end_date = (isset($params->end_date)) ? $params->end_date : null;
				
				if ($userId != null && $sportId != null && $name != null
						&& $description != null) {
					
					$em = $this->getDoctrine()->getManager();
					$user = $em->getRepository("BackendBundle:User")->findOneBy(array(
						"id" => $userId
					));
					$sport = $em->getRepository("BackendBundle:Sport")->findOneBy(array(
						"id" => $sportId
					));
					
					$club = $em->getRepository("BackendBundle:Club")->findOneBy(array(
						"id" => $clubId
					));
					
					$traing = new Traing();
					$traing -> setUser($user);
					$traing -> setSport($sport);
					$traing -> setClub($club);
					$traing -> setName($name);
					$traing -> setBody($body);
					$traing -> setCreatedAt($createdAt);
					$traing -> setDescription($description);
					$traing -> setTypeId($typeId);
					$traing ->setStatus($status);
					
					$em -> persist($traing);
					$em -> flush();
					
					$data = array(
						"status" => "success",
						"code" => 200,
						"msg" => "Traing created",
						"traing" => $traing
					);
					
				} else {
					$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "Traing not created"
					);
				}
				
			} else {
				$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "json not valid"
			);
			}
			
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "Authentication not valid"
			);
		}
		
		return $helpers -> json($data);
		
	}
	
	public function deleteAction(Request $request, $id=null) {
		
		$helpers = $this -> get("app.helpers");
		$hash = $request->get("auth", null);
		$authCheck = $helpers->authCheck($hash);
		
		if ($authCheck) {
			
			$identity = $helpers->authCheck($hash, true);
			$userId = (isset($identity->sub)) ? $identity->sub : null;
			
			$em = $this->getDoctrine()->getManager();
			$traing = $em->getRepository("BackendBundle:Traing")->find($id);
			
			if(is_object($traing) && $userId != null) {
				
				if(isset($identity->sub) && (
						$userId == $traing->getUser()->getId() || 
						$userId == $traing->getSport()->getUser()->getId())) {
					
					$traing -> setStatus("deleted");
					$em->persist($traing);
//					$em -> remove($traing);
					$em -> flush();
					
					$data = array(
						"status" => "success",
						"code" => 200,
						"msg" => "Traing deleted"
					);
					
				} else {
					$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "You are not the owner"
					);
				}
				
			} else {
				$data = array(
					"status" => "error",
					"code" => 400,
					"msg" => "Traing or User not found"
				);
			}
			
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "Authentication not valid"
			);
		}
		
		return $helpers -> json($data);
		
	}
	
	public function listAction(Request $request) {
		
		$helpers = $this -> get("app.helpers");
		$em = $this->getDoctrine()->getManager();
		$sportId = $request->get("sport_id", null);
		$clubId = $request->get("club_id", null);
		
		$sport = $em->getRepository("BackendBundle:Sport")->findOneBy(array(
			"id" => $sportId
		));
		
		$club = $em->getRepository("BackendBundle:Club")->findOneBy(array(
			"id" => $clubId
		));
		
		$traings = $em->getRepository("BackendBundle:Traing")->findBy(array(
			"sport" => $sport,
			"status" => array('public','private'),
			"club" => $club
		), array('id'=>'desc'));
		
//		$traings = $this->getDoctrine()->getRepository(Traing::class)->findAllActived($sport);
		
		if (count($traings) >= 1) {
			$data = array(
				"status" => "success",
				"code" => 200,
				"data" => $traings
			);
		
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "dont exist traings in this sport"
			);
		}
		
		return $helpers -> json($data);
	}
	
	public function detailAction(Request $request, $id=null) {
		$helpers = $this->get("app.helpers");
		$em = $this->getDoctrine()->getManager();
		$sportId = $request->get("sport_id", null);
		$clubId = $request->get("club_id", null);
		
		$sport = $em->getRepository("BackendBundle:Sport")->findOneBy(array(
			"id" => $sportId
		));
		
		$club = $em->getRepository("BackendBundle:Club")->findOneBy(array(
			"id" => $clubId
		));
		
		$traing = $em->getRepository("BackendBundle:Traing")->findOneBy(array(
			"id" => $id,
			"sport" => $sport,
			"club" => $club
		));
		
		if ($traing) {
			$data = array();
			$data["status"] = "success";
			$data["code"] = 200;
			$data["data"] = $traing;
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "Traing dont exist",
				"id" => $id,
				"sportId" => $sportId,
				"sport" => $sport
			);
		}
		
		return $helpers->json($data);
	}
	
	public function uploadImageAction(Request $request, $id) {
		$helpers = $this->get("app.helpers");
		
		$hash = $request->get("auth", null);
		$authCheck = $helpers->authCheck($hash);
		
		if ($authCheck == true) {
			
			$traingId = $id;
			
			$em = $this->getDoctrine()->getManager();
			$traing = $em->getRepository("BackendBundle:Traing")->findOneBy(array(
				"id" => $traingId	
			));
			
			//upload file
			$file = $request->files->get("image");
		
			if (!empty($file) && $file != null) {
				$ext = $file->guessExtension();
				if ($ext == "jpeg" || $ext == "jpg" || $ext == "png" || $ext == "gif") {
					$file_name = "traing_image_".time().".".$ext;
					$file->move("uploads/traings/" . $traing->getName() . "/images", $file_name);
					
					$traing->setFilePath($file_name);
					$em->persist($traing);
					$em->flush();

					$data = array(
						"status" => "success",
						"code" => 200,
						"msg" => "Image for traing upload success!"
					);
				} else {
					$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "File not valid"
					);
				}
			} else {
				$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "File not found",
						"file" => $file
					);
			}						
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "Image not upload"
			);
		}
		
		return $helpers->json($data);
	}
	
	public function findImagesAction(Request $request, $id=null) {
		$helpers = $this->get("app.helpers");
		//$images[];
		
		$hash = $request->get("auth", null);
		$authCheck = $helpers->authCheck($hash);
		
		if ($authCheck == true) {
			
			$em = $this->getDoctrine()->getManager();
			$traing = $em->getRepository("BackendBundle:Traing")->findOneBy(array(
				"id" => $id	
			));
			
			$path = "uploads/traing/" . $traing.getId() . "/images";
			$finder = new Finder();
			$finder->files()->in($path);
			
			foreach($finder as $file){
				$images[] = $file->getRelativePathname();
			}
		}
		
		return $helpers->json($images);
	}
	
}