<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;
use BackendBundle\Entity\Club;

class ClubController extends Controller {
	
	public function newAction(Request $request) {
		
		// helpers, json y auth
		$helpers = $this->get("app.helpers");		
		$json = $request->get("json", null);
		$params = json_decode($json);					

		if ($json != null) {

			// Datos de Club
			$em = $this->getDoctrine()->getManager();
			$createdAt = new \Datetime	("now");
			$image = "user-male.png";

			$name = (isset($params->name)) ? $params->name : null;
			$description = (isset($params->description)) ? $params->description : null;
			$image = (isset($params->image)) ? $params->image : $image;
			$origin = (isset($params->origin)) ? $params->origin : null;
			$creatorId = (isset($params->creator)) ? $params->creator : null;
			$status = (isset($params->status)) ? $params->status : "inactive";
			
			$creator = $em->getRepository("BackendBundle:User")->find($creatorId);

			if ($name != null && $name != "") {

				// Creamos un Club
				$club = new Club();
				$club->setName($name);
				$club->setDescription($description);
				$club->setImage($image);
				$club->setOrigin($origin);
				$club->setCreator($creator);
				$club->setCreatedAt($createdAt);
				$club->setStatus($status);

				// Se comprueba que el nombre no este en uso
				$isset_name = $em->getRepository("BackendBundle:Club")->findOneBy(array(
					"name" => $name
				));

				if (count($isset_name)===0) {

					// Guardar y terminar					
					$em -> persist($club);
					$em -> flush();	
					
					$creator->setClub($club);
					$em -> persist($creator);
					$em -> flush();					

					$data["status"] = "success";
					$data["code"]   = 200;	
					$data["msg"]    = "New Club created";
					$data["data"]	= $club;
					
					

				} else {
					$data = array(
						"status" => "error",
						"code" => 400,
						"msg" => "name already exist"
					);
				}

			} else {
				$data = array(
					"status" => "error",
					"code" => 400,
					"msg" => "json invalid"
				);
			}

		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "json not found"
			);
		}
			
		return $helpers->json($data);
	}

	public function editAction(Request $request, $id=null) {
		
		// helpers, json y auth
		$helpers = $this->get("app.helpers");
		$json = $request->get("json", null);
		$params = json_decode($json);
		$hash = $request->get("auth", null);		
		$authCheck = $helpers->authCheck($hash);
		
		if ($authCheck) {
			
			$identity = $helpers->authCheck($hash, true);
			
			if ($json != null) {
								
				$updatedAt = new \DateTime("now");
				$name = (isset($params->name)) ? $params->name : null;
				$description = (isset($params->description)) ? $params->description : null;
				$origin = (isset($params->origin)) ? $params->origin : null;
				$status = (isset($params->status)) ? $params->status : null;
				
				$em = $this->getDoctrine()->getManager();
				$club = $em ->getRepository("BackendBundle:Club")->findOneBy(array(
					"id" => $id
				));
				
				if (!count($club)==0) {
				
					if (isset($identity->sub) && $identity->sub == $club->getCreator()->getId()) {
						
						$club->setUpdateAt($updatedAt);
						$club->setName($name);				
						$club->setDescription($description);
						$club->setOrigin($origin);
						$club->setStatus($status);														
						
						$em -> persist($club);
						$em -> flush();
						$data = array(
							"status"=> "success",
							"code"	=> 200,
							"name" => $name,
							"data"	=> $club							
						);

					} else {
						$data = array(
							"status" => "error",
							"code" => 400,
							"msg" => "dont have permision"
						);
					}
					
				} else {
						$data = array(
							"status" => "error",
							"code" => 400,
							"msg" => "Club dont found"
						);
					}
				
			} else {
				$data = array(
					"status" => "error",
					"code" => 400,
					"msg" => "json not found"
				);
			}
			
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "authorization failed"
			);
		}
		
		return $helpers->json($data);
	}
	
	public function detailAction(Request $request, $name=null){
		
		// helpers y auth
		$helpers = $this->get('app.helpers');
		$hash = $request->get('auth', null);
		$authcheck = $helpers->authCheck($hash);
		
		if ($authcheck) {
			
			// Se busca al club
			$em = $this->getDoctrine()->getManager();
			$club = $em->getRepository("BackendBundle:Club")->findOneBy(array(
				"name" => $name
			));
			
			if ($club) {
				$data = array(
					"status" => "success",
					"code" => "200",
					"data" => $club
				);
				
			} else {
				$data = array(
					"status" => "error",
					"code" => "400",
					"msg" => "club not found"
				);
			}
			
		} else {
			$data = array(
				"status" => "error",
				"code" => "400",
				"msg" => "authorization not valid"
			);
		}
		
		return $helpers ->json($data);
	}
	
	public function detailIdAction(Request $request, $id=null){
		
		// helpers y auth
		$helpers = $this->get('app.helpers');
			
		// Se busca al club
		$em = $this->getDoctrine()->getManager();
		$club = $em->getRepository("BackendBundle:Club")->find($id);

		if ($club) {
			$data = array(
				"status" => "success",
				"code" => "200",
				"data" => $club
			);

		} else {
			$data = array(
				"status" => "error",
				"code" => "400",
				"msg" => "club not found"
			);
		}
		
		return $helpers ->json($data);
	}	

	public function listAction(Request $request) {
		
		// helpers, json y auth
		$helpers = $this->get("app.helpers");		
		$hash = $request->get("auth", null);
		$authCheck = $helpers->authCheck($hash);

		if($authCheck) {
			
			// Se buscan todos los clubs
			$em = $this->getDoctrine()->getManager();
			$clubs = $em->getRepository("BackendBundle:Club")->findAll();
			
			
			if (!count($clubs)==0) {
				
				$data = array(
					"status" => "success",
					"code"   => 200,
					"data"	 => $clubs
				);
				
			} else {
				$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "clubs dont found"
			);
		}
			
		} else {
			$data = array(
				"status" => "error",
				"code" => 400,
				"msg" => "authorization failed"
			);
		}
		
		return $helpers->json($data);
	}
	
	public function checkNameAction(Request $request, $name=null) {
		$helpers = $this->get("app.helpers");
		
		$em = $this->getDoctrine()->getManager();
		$clubs = $em->getRepository("BackendBundle:Club")->findAll();
		$name = $this->getRepository("BackendBundle:Club")->findOneBy(array(
				"name" => $name
			));
		
		if ($name != null || $name != "") {
			$data = true;
		}
		else {
			$data = false;
		}
		
		return $helpers->json($data);
	}
}