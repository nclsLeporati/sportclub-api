
CREATE DATABASE IF NOT EXISTS club_app;
USE club_app;

CREATE TABLE user(
id          int(255) auto_increment not null,
role        varchar(20)  not null,
run			varchar(12),
name        varchar(255) not null,
lastname    varchar(255) not null,
email       varchar(255) not null,
cellnumber  int(9),
password    varchar(255) not null,
image       varchar(255),
birth_date  datetime,
origin		varchar(255),
nacionality varchar(255),
origin_club	varchar(255),
created_at  datetime,
updated_at	datetime,
status      varchar(20),
club_id		int(255),
CONSTRAINT pk_user PRIMARY KEY(id)
)ENGINE=InnoDb;

CREATE TABLE club(
id          int(255) auto_increment not null,
name        varchar(20)  not null,
description	varchar(12),
image       varchar(255),
origin		varchar(255),
created_at  datetime,
update_at	datetime,
status      varchar(20),
creator_id	int(255),
CONSTRAINT pk_club PRIMARY KEY(id),
CONSTRAINT fk_club_creator FOREIGN KEY(creator_id) REFERENCES user(id)
)ENGINE=InnoDb;

ALTER TABLE user ADD CONSTRAINT fk_user_club FOREIGN KEY(club_id) REFERENCES club(id);

CREATE TABLE sport(
id          int(255) auto_increment not null,
name        varchar(20) not null,
description varchar(255) not null,
image       varchar(255),
created_at  datetime not null,
updated_at  datetime,
status      varchar(20),
creator_id     int(255),
CONSTRAINT pk_sport PRIMARY KEY(id),
CONSTRAINT fk_sport_creator FOREIGN KEY(creator_id) REFERENCES user(id)
)ENGINE=InnoDb;

CREATE TABLE team(
id          int(255) auto_increment not null,
name        varchar(20) not null,
image       varchar(255),
created_at  datetime not null,
updated_at  datetime,
status      varchar(20),
sport_id    int(255) not null,
creator_id 	int(255) not null,
description varchar(255),
CONSTRAINT pk_team PRIMARY KEY(id),
CONSTRAINT fk_team_sport FOREIGN KEY(sport_id) REFERENCES sport(id),
CONSTRAINT fk_team_creator FOREIGN KEY(creator_id) REFERENCES user(id)
)ENGINE=InnoDb;

CREATE TABLE traing(
id          int(255) auto_increment not null,
name        varchar(20) not null,
description varchar(255) not null,
body        text,
time		int(20),
created_at  datetime not null,
updated_at  datetime,
status      varchar(20),
user_id     int(255) not null,
sport_id    int(255) not null,
type_id     int(255) not null,
club_id		int(255) not null,
CONSTRAINT pk_traing PRIMARY KEY(id),
CONSTRAINT fk_traing_user FOREIGN KEY(user_id) REFERENCES user(id),
CONSTRAINT fk_traing_sport FOREIGN KEY(sport_id) REFERENCES sport(id),
CONSTRAINT fk_traing_club FOREIGN KEY(club_id) REFERENCES club(id)
)ENGINE=InnoDb;

CREATE TABLE clndrEvent(
id          int	(255) auto_increment not null,
start_date  datetime not null,
end_date  	datetime,
duration	int(255),
name        varchar (20) not null,
description varchar (255),
locale      varchar(255),
created_at  datetime not null,
updated_at  datetime,
creator_id     int(255) not null,
type     	int(255) not null,
status      varchar(20),
CONSTRAINT pk_event PRIMARY KEY(id),
CONSTRAINT fk_event_creator FOREIGN KEY(creator_id) REFERENCES user(id)
)ENGINE=InnoDb;

CREATE TABLE media(
id          int		(255) 	auto_increment	not null,
type		varchar	(255) 	not null,
name        varchar	(20) 	not null,
text		varchar	(255),
filePath	varchar	(255) 	not null,
ext			varchar	(10) 	not null,
status      varchar	(20),
restriction	varchar (20),
upload_at 	datetime		not null,
update_at 	datetime		not null,
owner_id	int		(255) 	not null,
CONSTRAINT pk_media PRIMARY KEY(id),
CONSTRAINT fk_media_owner FOREIGN KEY(owner_id) REFERENCES user(id)
)ENGINE=InnoDb;

CREATE TABLE sport_stat(
id          int		(255) 	auto_increment	not null,
name		varchar(255)	not null,
chort		varchar(5)		not null,
description varchar(255)	not null,
sport_id	int	(255)		not null,
CONSTRAINT pk_sport_stat PRIMARY KEY(id),
CONSTRAINT fk_sport_stat FOREIGN KEY(sport_id) REFERENCES sport(id)
)ENGINE=InnoDb;

CREATE TABLE user_stat(
id          int		(255) 	auto_increment	not null,
value		varchar(20)		not null,
user_id		int	(255)		not null,
sport_stat_id	int	(255)	not null,
CONSTRAINT pk_user_stat PRIMARY KEY(id),
CONSTRAINT fk_user_stat FOREIGN KEY(user_id) REFERENCES user(id),
CONSTRAINT fk_user_sport_stat FOREIGN KEY(sport_stat_id) REFERENCES sport_stat(id)
)ENGINE=InnoDb;

CREATE TABLE user_sport(
user_id		int(255)	not null,
sport_id	int(255)	not null,
CONSTRAINT pk_user_sport PRIMARY KEY (user_id, sport_id),
CONSTRAINT fk_user FOREIGN KEY(user_id) REFERENCES user(id),
CONSTRAINT fk_sport FOREIGN KEY(sport_id) REFERENCES sport(id)
)ENGINE=InnoDb;